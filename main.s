global _start 

;; R8 ..... Current opcode
;; R9 ..... PC/IP
;; R10 .... SP
;; R11 .... Memory index
;; R12 .... Program size
;; R13 .... Termination flag
;; R14 .... General purpose 

section .data
	err_no_file:		db "Error: no file specified", 			0x0A, 0x00
	err_bad_file:		db "Error: unable to open file for reading", 	0x0A, 0x00
	err_bad_read:		db "Error: unable to read file", 		0x0A, 0x00
	err_bad_num_args:	db "Error: incorrect number of arguments", 	0x0A, 0x00
	err_bad_op:		db "Error: bad operation encountered", 		0x0A, 0x00
		
	prompt:			db 0x0A, "> ", 0x00
	newline:		db 0x0A

	debug_inc_ptr:		db "inc ptr", 	0x0A, 0x00
	debug_dec_ptr:		db "dec ptr", 	0x0A, 0x00
	debug_inc_mem:		db "inc mem", 	0x0A, 0x00
	debug_dec_mem:		db "dec mem", 	0x0A, 0x00
	debug_prn:		db "print", 	0x0A, 0x00
	debug_input:		db "input", 	0x0A, 0x00
	debug_loopb:		db "loop beg", 	0x0A, 0x00
	debug_loope:		db "loop end", 	0x0A, 0x00

section .bss
	file_data:	resb 8096
	bf_memory:	resb 30000
	input:		resb 2 

section .text
	_start:
		;; Pop the number of arguments off the stack
		pop	rax
		cmp	rax, 2
		je	arg_num_good
		
		;; If argv != 2, terminate the program
		mov	r14, err_bad_num_args
		jmp	fail

	arg_num_good:

		;; Open the file in readonly mode
		mov	rax, 2		;; open
		pop	rdi
		pop	rdi		;; pathname
		xor	rsi, rsi	;; O_RDONLY
		syscall

		;; If we are unable to open the file, terminate the program
		cmp	rax, 0
		jge	open_good
		mov	r14, err_bad_file 
		call	fail

	open_good:
		
		;; Read the file into memory
		push 	rax	
		xor	rax, rax
		pop	rdi			;; descriptor
		mov	rsi, file_data		;; buffer
		mov	rdx, 8096		;; length
		syscall

		mov	r12, rax
		dec	r12

		;; Terminate the program if we were unable to read the file
		cmp	rax, 0
		jge	read_good
		mov	r14, err_bad_read
		call	fail

	read_good:

		;; Clear the interpreter's memory
		mov	rcx, 29999
	init_loop:
		mov	byte [ bf_memory + rcx ], 0
		loop	init_loop

		;; Make sure these registers are empty
		xor	r9, r9
		xor	r10, r10
		xor	r11, r11
		xor	r13, r13
		;; Begin the main interpreter loop
	interpret:
		cmp	r9, r12		;; Make sure r9 is within the size of the file
		jge	bf_end
		mov	r8b, [ file_data + r9 ]
		jmp	bf_run_cycle

	bf_end:
		;; Exit
		mov	rax, 60
		xor	rdi, rdi
		syscall

	bf_run_cycle:
		inc	r9
		cmp	r8b, 0x3C	;; <
		je	op_dec_ptr
		cmp	r8b, 0x3E	;; >
		je	op_inc_ptr
		cmp	r8b, 0x2B	;; +
		je	op_inc_mem
		cmp	r8b, 0x2D	;; -
		je	op_dec_mem
		cmp	r8b, 0x2C	;; ,
		je	op_input
		cmp	r8b, 0x2E	;; .
		je	op_print
		cmp	r8b, 0x5B	;; [
		je	op_loop_beg
		cmp	r8b, 0x5D	;; ]
		je	op_loop_end
		jmp	interpret
		
	;; Decrement the memory pointer	
	op_dec_ptr:
		mov	r14, debug_dec_ptr
		call	debug_print

		cmp	r11, 0
		jg	odp_done
		mov	r11, 30000
		
	odp_done:
		dec	r11
		jmp	interpret

		
	;; Increment the memory pointer
	op_inc_ptr:
		mov	r14, debug_inc_ptr
		call	debug_print

		inc	r11
		cmp	r11, 30000
		jl	interpret
		xor	r11, r11
		jmp	interpret
		
	;; Increment the currently selected byte
	op_inc_mem:
		mov	r14, debug_inc_mem
		call	debug_print

		inc	byte [ bf_memory + r11 ]
		jmp	interpret


	;; Decrement the currently selected byte
	op_dec_mem:
		mov	r14, debug_dec_mem
		call	debug_print

		dec	byte [ bf_memory + r11 ]
		jmp	interpret


	;; Get user input
	op_input:
		push	r11	;; Preserve r11 as it becomes clobbered by read/write
		
		mov	r14, debug_input
		call	debug_print

		;; Get the length of the prompt
		mov	r14, prompt
		call	strlen
		push	rax

		;; Print the prompt
		mov	rax, 1
		mov	rdi, 1
		mov	rsi, prompt
		pop	rdx
		syscall

		;; Read the user input
		xor	rax, rax		;; read
		xor	rdi, rdi		;; stdin
		mov	rsi, input
		mov	rdx, 2			;; size (char + newline)
		syscall

		pop	r11
		
		mov	r14b, [ input ]			;; Grab the first byte from the input
		mov	[ bf_memory + r11 ], r14b	;; Load the byte into memory

		jmp	interpret


	;; Print the currently selected byte
	op_print:
		push	r11
		
		mov	r14, debug_prn
		call	debug_print

		mov	rax, 1			;; write
		mov	rdi, 1			;; stdout
		lea	rsi, [ bf_memory + r11 ]
		mov	rdx, 1			;; size
		syscall

		pop	r11

		jmp	interpret


	;; Loop begin
	op_loop_beg:
		mov	r14, debug_loopb
		call	debug_print

		;; Do we need to loop? If the current byte is 0, skip the loop
		cmp	byte [ bf_memory + r11 ], 0
		jne	interpret
		
		mov	r14, 1

	;; Check for the end of the loop and jump to it
	lp_beg_lp:
		cmp	r14, 0
		jne	interpret
		inc	r9
		cmp	byte [ file_data + r9 ], 0x5D	;; ]
		je	lp_beg_dec
		cmp	byte [ file_data + r9 ], 0x5B	;; [
		je	lp_beg_inc
		jmp	lp_beg_lp

	lp_beg_inc:
		inc	r14
		jmp	lp_beg_lp

	lp_beg_dec:
		dec	r14
		jmp	lp_beg_lp


	;; Loop end
	op_loop_end:
		mov	r14, debug_loope
		call	debug_print

		cmp	byte [ bf_memory + r11 ], 0
		je	interpret
		
		mov	r14, 1
		dec	r9	;; Account for the fact that r9 has been incremented

	;; Check for the start of the loop and jump to it
	lp_end_lp:
		cmp	r14, 0
		je	interpret
		dec	r9
		cmp	byte [ file_data + r9 ], 0x5D	;; ]
		je	lp_end_inc
		cmp	byte [ file_data + r9 ], 0x5B	;; [
		je	lp_end_dec
		jmp	lp_end_lp

	lp_end_done:
		dec	r9
		jmp	interpret

	lp_end_inc:
		inc	r14
		jmp	lp_end_lp

	lp_end_dec:
		dec	r14
		jmp	lp_end_lp


	;; Bad operation
	bad_op:
		mov	r14, err_bad_op
		call	fail


	;; Print an error message & quit
	fail:
		;; Get the length of the error message
		call	strlen
		push	rax

		;; Print the error
		mov	rax, 1
		mov	rdi, 2
		mov	rsi, r14
		pop	rdx
		syscall

		;; Exit
		mov	rax, 60
		mov	rdi, 1
		syscall


	;; Print debug information
	debug_print:
		push	r11

		call	strlen
		mov	rdx, rax

		mov	rax, 1
		mov	rdi, 1
		mov	rsi, r14
		; syscall
		
		pop	r11
		
		ret


	;; Get the length of a null-terminated string
	strlen:
		xor	rax, rax
	sl_loop:
		cmp	byte [ r14 + rax ], 0	;; Check the RAXth byte of the string for null
		je	sl_done
		inc	rax
		jmp	sl_loop
	sl_done:
		ret
