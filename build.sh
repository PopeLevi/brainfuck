#!/bin/bash

nasm main.s -o main.o -felf64 -g
if [ $? == 1 ]; then exit; fi
ld main.o -o brainfuck -g -O2
