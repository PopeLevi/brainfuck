global alloc
global realloc
global free
global strlen

section .data
	file_dn:	db "/dev/zero", 0x00

section .text
	alloc:
		cmp	rax, 0
		je	alloc_end

		push	r10
		push	r8
		push	r9
		push	r14	;; length

		;; Open /dev/zero for output
		mov	rax, 2
		mov	rdi, file_dn
		mov	rsi, 2		;; O_RDWR
		syscall

		mov	r14, rax

		;; mmap
		mov	rax, 9
		xor	rdi, rdi	;; address (let the kernel handle this)
		pop	rsi		;; length
		mov	rdx, 3		;; PROT_READ | PROT_WRITE 
		mov	r10, 2		;; MAP_PRIVATE
		mov	r8, r14		;; descriptor
		xor	r9, r9		;; offset
		syscall

		;; Reset the register values
		pop	r9
		pop	r8
		pop	r10

		alloc_end:
		ret
	
	realloc:
		push	r14
		push	rax
		
		;; Remap
		mov	rax, 25		;; mremap
		mov	rdi, r14	;; Old address
		mov	rsi, r15	;; Old size
		realloc_end:
		ret

	free:
		mov	rdi, rax	;; Memory
		mov	rax, 11		;; munmap
		mov	rsi, r14	;; Length
		syscall
		ret
		
	strlen:
		xor	rax, rax
		dec	rax
		strlp:
		inc	rax
		cmp	byte [ r14 + rax ], 0
		jne	strlp
		ret
